﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    [SerializeField]
    Text scoreLabel = null;

    [SerializeField]
    SettingPopup settingsPopup = null;

    private int _score = 0;

	// Use this for initialization
	void Start () {
        scoreLabel.text = Time.realtimeSinceStartup.ToString();
        settingsPopup.Close();
    }
	
	// Update is called once per frame
	//void Update () {
		
	//}

    void Awake()
    {
        Messenger.AddListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    public void OnOpenSettings()
    {
        Debug.Log("Open setting");
        settingsPopup.Open();
    }

    public void OnPointerDown()
    {
        Debug.Log("Pointer down");
    }



    void OnEnemyHit()
    {
        _score += 1;
        scoreLabel.text = _score.ToString();
    }

}
