﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInput : MonoBehaviour {

    public float baseSpeed = 6.0f;
    private float speed = 6.0f;
    public float gravity = -9.8f;

    private CharacterController _charController;

	// Use this for initialization
	void Start () {
        _charController = GetComponent<CharacterController>();
	}

    void Awake()
    {
        Messenger<float>.AddListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    void OnDestroy()
    {
        Messenger<float>.RemoveListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    // Update is called once per frame
    void Update () {
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;
        Vector3 movment = new Vector3(deltaX, 0, deltaZ);
        movment = Vector3.ClampMagnitude(movment, speed);
        movment.y = gravity;
        movment *= Time.deltaTime;
        movment = transform.TransformDirection(movment);
        _charController.Move(movment);
	}

    void OnSpeedChanged(float value)
    {
        speed = baseSpeed * value;
    }
}
