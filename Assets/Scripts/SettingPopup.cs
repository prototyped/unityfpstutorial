﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPopup : MonoBehaviour {

    [SerializeField]
    Slider speedSlider = null;

	// Use this for initialization
	void Start () {
        speedSlider.value = PlayerPrefs.GetFloat("speed", 1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void OnSummitName(string name)
    {
        Debug.Log("Summit " + name);
        PlayerPrefs.SetString("name", name);
    }

    public void OnSpeedValue(float speed)
    {
        Messenger<float>.Broadcast(GameEvent.SPEED_CHANGED, speed);
        Debug.Log("Speed: " + speed);
        PlayerPrefs.SetFloat("speed", speed);
    }
}
